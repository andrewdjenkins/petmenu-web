/**
 *
 * spud.js - how many times has potato been served?
 *
 * @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 * Copyright (C) 2020  Andrew Jenkins
 *
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 *
 */

function dd(i) {
    if (i.length == 1) i = "0" + i;
    return i;
}

function dateToStr(d) {
    const tMon = dd((d.getMonth()+1).toString());
    const tDay = dd(d.getDate().toString());
    return (d.getFullYear() + "-" + tMon + "-" + tDay);
}

function parsedatestr(s) {
    
    return s.split("-").map(a => Math.round(a));
}

function strToDate(d) {
    const ymd = parsedatestr(d)
    return new Date(ymd[0], ymd[1] - 1, ymd[2]);
}

function spudCount(menuItem) {
    // the second part being the dietary requirements list.
    const itemText = menuItem[0].toLowerCase();
    if (itemText.includes("potato") 
        || itemText == "chips"
        || itemText == "croquettes"
        || itemText.includes("hash brown")) {
        return 1;
    } else if (itemText.includes("fries")) {
        // these aren't guaranteed to include potato.
        console.log("assuming potato fries: " + itemText);
        return 1;
    } else return 0;
}

function spudCountL(itemList) {
    return itemList.reduce((acc, cur) => acc + spudCount(cur), 0);
}

function spudCountLunch(menuday) {
    if (!menuday.hasOwnProperty("lunch")) return 0;
    return (spudCount(menuday.soup)
            + spudCountL(menuday.lunch.mains)
            + spudCountL(menuday.lunch.sides)
            + spudCountL(menuday.potato)
            + spudCountL(menuday.desserts)) // very unlikely but worth including
}

function spudCountDinner(menuday) {
    if (!menuday.hasOwnProperty("dinner")) return 0;
    return (spudCountL(menuday.dinner.mains)
            + spudCountL(menuday.dinner.sides)
            + spudCountL(menuday.desserts)) // very unlikely but worth including
}

function spudCountBrunch(menuday) {
    if (!menuday.hasOwnProperty("brunch")) return 0;
    let s = spudCountL(menuday.brunch.regular);
    if (menuday.hasOwnProperty("specials")) {
        console.log("found brunch special");
        s += spudCountL(menuday.brunch.specials);
    }
    return s;
}

function daysBeforeDay(d1, ds) {
    return ds.filter(d => d<d1);
}

function isBreakfastDay(d) {
    let dow = d.getDay();
    // Sun is 0, Sat is 6
    return (dow>=1 && dow<=5);
}

function spudCountBeforeDay(d1, data) {
    
    const menus_by_date = data.menus_by_date;
    const spudCountBreakfast = spudCountL(data.breakfast);
    const days = daysBeforeDay(d1, Object.keys(menus_by_date));
    let spuds = 0;
    days.forEach(d => {
        spuds += spudCountLunch(menus_by_date[d])
                 + spudCountBrunch(menus_by_date[d]) 
                 + spudCountDinner(menus_by_date[d]);
        if (isBreakfastDay(strToDate(d))) {
            spuds += spudCountBreakfast;
        }
    });
    return spuds;
}

const mealtimes = {
    breakfast_start : 8,
    breakfast_end : 9.5,
    brunch_start : 10,
    brunch_end : 13.5,
    lunch_start : 11.75,
    lunch_end : 13.5,
    dinner_start : 17.75,
    dinner_end : 18.75
}

function floatHour(d) {
    return d.getHours() + (d.getMinutes() / 60)
}

// get the dinner end time.
function strDatePlusTime(mealdate, time) {
    const ymd = parsedatestr(mealdate);
    
    let d = new Date(ymd[0], ymd[1] - 1, ymd[2]);
    let hours = Math.floor(time); 
    
    d.setHours(hours);
    d.setMinutes((time - hours) * 60);
    
    return d;
}

function dayBefore(d) {
    return new Date(d - 24 * 3600 * 1000);
}

function timeSinceLastSpud(dtnow, data) {
    const mealdays = data.menus_by_date;
    
    let refdate = dtnow;
    let key = dateToStr(refdate);
    let hour = floatHour(dtnow); 
    let dt = 0;
    
    while (mealdays.hasOwnProperty(key)) {
        if ((hour >= mealtimes.dinner_start)
            && spudCountDinner(mealdays[key])){
            /* if our REFERENCE MEAL is dinner and this reference meal contained 
               potato then return time since this reference meal */
            dt = dtnow.getTime() - strDatePlusTime(key, mealtimes.dinner_end).getTime();
            if (dt < 0) return 0 
            else return dt
        } else if ((hour >= mealtimes.lunch_start)
            && spudCountLunch(mealdays[key])) {
            dt = dtnow.getTime() - strDatePlusTime(key, mealtimes.lunch_end).getTime();
            if (dt < 0) return 0 
            else return dt
        } else if ((hour >= mealtimes.brunch_start)
            && spudCountBrunch(mealdays[key])) {
            dt = dtnow.getTime() - strDatePlusTime(key, mealtimes.brunch_end).getTime();
            if (dt < 0) return 0 
            else return dt
        } else if ((hour >= mealtimes.breakfast_start)
            && spudCountL(data.breakfast) && isBreakfastDay(refdate)){
            dt = dtnow.getTime() - strDatePlusTime(key, mealtimes.breakfast_end).getTime();
            if (dt < 0) return 0 
            else return dt
        } else {
            if (hour >= mealtimes.dinner_start) {
                /* if we were looking at dinner then set the reference meal to 
                   whichever is later of lunch and brunch */
                hour = Math.max(mealtimes.lunch_end, mealtimes.brunch_end);
            } else if (hour >= mealtimes.lunch_start) {
                // if looking at lunch then look at breakfast
                hour = mealtimes.breakfast_end;
            } else {
                // otherwise set the reference meal to dinner the day before
                hour = mealtimes.dinner_end;
                refdate = dayBefore(refdate);
                key = dateToStr(refdate);
            }
        }
    }
    // no spuds found.
    return null;
}

function updateSpudTimer(data) {
    let now = new Date(Date.now());
    let millis = timeSinceLastSpud(now, data);
    let timerstr;
    let timeout;
    let s = "";
    let secs = Math.floor(millis / 1000)
    if (secs < 60) {
        // using != because '0 seconds', '1 second' etc
        if (secs != 1) s="s";
        timestr = secs + " second" + s;
        timeout = 100;
    } else {
        let mins = Math.floor(secs / 60)
        if (mins < 60) {
            if (mins != 1) s="s";
            timestr = mins + " minute" + s;
            timeout = 5000;
        } else {
            let hours = Math.floor(mins / 60)
            if (hours != 1) s="s";
            timestr = hours + " hour" + s;
            timeout = 60000;
        }
    }
    document.getElementById("spudtimer").firstChild.textContent = timestr;
    window.setTimeout((() => updateSpudTimer(data)), timeout);
}

function setSpudCounts(data) {
    let now = new Date(Date.now());
    let today = dateToStr(now);
    updateSpudTimer(data);
    let time = floatHour(now);
    let n_days = daysBeforeDay(today, Object.keys(data.menus_by_date)).length + 1;
    let n_spuds = spudCountBeforeDay(today, data);
    
    // also include today's meals, if they have happened yet.
    if (time >= mealtimes.dinner_start) {
        console.log("adding today's dinner");
        n_spuds += spudCountDinner(data.menus_by_date[today]);
    }
    if (time >= mealtimes.lunch_start) {
        console.log("adding today's lunch");
        n_spuds += spudCountLunch(data.menus_by_date[today]);
    }
    if (time >= mealtimes.brunch_start) {
        console.log("adding (potential) brunch")
        n_spuds += spudCountBrunch(data.menus_by_date[today]);
    }
    if ((time >= mealtimes.breakfast_start) && isBreakfastDay(now)) {
        console.log("adding today's breakfast");
        n_spuds += spudCountL(data.breakfast);
    }
    
    let spud_rate = n_spuds / n_days;
    if (n_spuds == 1) n_spuds = "once";
    else if (n_spuds == 2) n_spuds = "twice";
    else n_spuds = n_spuds + "\xA0times";

    document.getElementById("spudcount").firstChild.textContent = n_spuds;
    document.getElementById("spudrate").firstChild.textContent = spud_rate.toFixed(2);
}

fetch("servery.json")
    .then(response => response.json())
    .then(d => setSpudCounts(d));
