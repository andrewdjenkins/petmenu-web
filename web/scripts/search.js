// these bits have been lifted from main.js
function dd(i) {
    if (i.length == 1) i = "0" + i;
    return i;
}

function dateToLabel(d){
    days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
              "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    return days[d.getDay()] + " " + d.getDate()
           + " " + months[d.getMonth()];
}

function dateToLongLabel(d){
    days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday",
            "Friday", "Saturday"];
    months = ["January", "February", "March", "April", "May", "June",
              "July", "August", "September", "October", "November",
              "December"];
    return days[d.getDay()] + " " + d.getDate()
           + " " + months[d.getMonth()];
}

function parseDateStr(s) {
    return s.split("-").map(a => Math.round(a));
}

function strToDate(d) {
    const ymd = parseDateStr(d)
    return new Date(ymd[0], ymd[1] - 1, ymd[2]);
}

function searchDate (d, s) {
	s = s.toLowerCase();
	let menu = data.menus_by_date[d]
	let lunchResults = [];
	let dinnerResults = [];
	let brunchResults = [];
	if (menu.lunch) {
		/* mains and sides are a list of (name, DI) pairs */
		menu.lunch.mains.forEach(x => { 
			if (x[0].toLowerCase().search(s)!=-1) 
				lunchResults.push(x[0])
		})
		menu.lunch.sides.forEach(x => {
			if (x[0].toLowerCase().search(s)!=-1) 
				lunchResults.push(x[0])
		})
		if (menu.soup && menu.soup[0].toLowerCase().search(s)!=-1) 
			lunchResults.push(menu.soup[0]);
	}
	if (menu.dinner) {
		/* mains and sides are a list of (name, DI) pairs */
		menu.dinner.mains.forEach(x => { 
			if (x[0].toLowerCase().search(s)!=-1) 
				dinnerResults.push(x[0])
		})
		menu.dinner.sides.forEach(x => {
			if (x[0].toLowerCase().search(s)!=-1) 
				dinnerResults.push(x[0])
		})
		menu.desserts.forEach(x => {
			if (x[0].toLowerCase().search(s)!=-1) 
				dinnerResults.push(x[0])
		})
	}
	if (menu.brunch) {
		/* mains and sides are a list of (name, DI) pairs */
		menu.brunch.regular.forEach(x => { 
			if (x[0].toLowerCase().search(s)!=-1) 
				brunchResults.push(x[0])
		})
		if (menu.brunch.specials) {
			menu.brunch.specials.forEach(x => {
				if (x[0].toLowerCase().search(s)!=-1) 
					brunchResults.push(x[0])
			})
		}
	}
	if (lunchResults.length || dinnerResults.length || brunchResults.length) {
		var output = document.getElementById("results").appendChild(document.createElement("div"))
		output.appendChild(document.createElement("h2")).textContent = dateToLabel(strToDate(d));
	}
	/* there's quite a bit of repeated code here which could definitely be put into a function at some point */
	if (brunchResults.length) {
		let x = document.createElement("h3");
		x.textContent = "Brunch";
		output.appendChild(x);
		let l = output.appendChild(document.createElement("ul"));
		brunchResults.forEach(r => {
			x = document.createElement("li");
			x.appendChild(document.createTextNode(r));
			l.appendChild(x);
		})
	}
	if (lunchResults.length) {
		x = document.createElement("h3");
		x.textContent = "Lunch";
		output.appendChild(x);
		let l = output.appendChild(document.createElement("ul"));
		lunchResults.forEach(r => {
			x = document.createElement("li");
			x.appendChild(document.createTextNode(r));
			l.appendChild(x);
		})
	}
	if (dinnerResults.length) {
		let x = document.createElement("h3");
		x.textContent = "Dinner";
		output.appendChild(x);
		let l = output.appendChild(document.createElement("ul"));
		dinnerResults.forEach(r => {
			x = document.createElement("li");
			x.appendChild(document.createTextNode(r));
			l.appendChild(x);
		})
	}

}

function search(s) {
	let results = document.getElementById("results");
	while (results.firstChild) results.firstChild.remove();
	if (!s) return;
	let today = new Date(Date.now());
	let tMon = dd((today.getMonth()+1).toString());
	let tDay = dd((today.getDate()).toString());

	today = (today.getFullYear() + "-" + tMon + "-" + tDay);
	console.log(today);
	let dates = Object.keys(data.menus_by_date).filter(function (x) {return x>=today});
	dates.forEach(d => {searchDate(d, s)})
}

var data;

document.getElementById("searchbox").addEventListener("input", e => search(e.target.value));
fetch("servery.json")
	.then(r=>r.json())
	.then(d=>{data=d;
		let e = document.getElementById("searchbox");
		let s = new URLSearchParams(document.location.search);
		e.value=s.get("q");
		search(e.value)
	})
