# petmenu-web

Unofficial Peterhouse menu site based on JSON menu data.

It is hoped that this might equally be useful for other colleges/universities.

## Usage:

1. Place the contents of `web/` and the JSON menu data files `servery.json` and 
   `formal.json` at the desired location on the web server.

## License:

This project is released under the the GNU General Public License version 3
(file `COPYING3`) or, (at your option), any later version. The Javascript files
have an additional permission to minimize & compact as per 
https://www.gnu.org/software/librejs/free-your-javascript.html
